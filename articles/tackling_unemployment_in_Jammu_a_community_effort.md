# Tackling Unemployment in Jammu: A Community Effort
***Kalhan Bhat, Ansh Sharma, Krishika Jalali, Rushikesh Salgotra, Prinka Devi, and Keshav Kumar,**
B. Tech Students at Sardar Patel Institute of Technology, Mumbai*

## Introduction

Unemployment remains a big challenge in our community, impacting individuals and families across Jammu City. By understanding the root causes and actively working on solutions, we can make a positive change. This article explores the current state of unemployment in our community, highlights recent survey findings, and outlines effective strategies to combat it.
As residents of Jammu and B Tech students at Sardar Patel Institute of Technology, Mumbai, my teammates Ansh Sharma, Krishika Jalali, Rushikesh Salgotra, Prinka Devi, Keshav, and I have taken on this project to address the pressing issue of unemployment in our city.

## Understanding Unemployment
*Kalhan, Ansh & Rushikesh*

Unemployment isn’t just about a lack of jobs; it’s a complex issue influenced by economic conditions, skills mismatch, and market dynamics. In Jammu City, the unemployment rate has been worrying, with many individuals struggling to find stable employment.

*"The greatest glory in living lies not in never falling, but in rising every time we fall."*
*- Nelson Mandela*

## Survey Findings
A recent survey in our community highlighted several key issues contributing to unemployment:

### Direct Train to Srinagar (Starting December 2024)
- Reduced Tourist Traffic in Jammu: The new train service will enable tourists to bypass Jammu, affecting local tourism revenue.
- Shift in Trade and Commerce: There could be a reduction in demand for businesses reliant on goods and passenger transit through Jammu.

![This is an alt text.](/images/unemp/train.webp "This is a sample image.")

### E-Commerce
- Decline in Local Market Sales: Online shopping is reducing foot traffic and the competitiveness of local SMEs.
- Job Losses: Reduced sales are causing layoffs in the retail sector and affecting supporting services.

### Unemployment Benefits
- Policy Gaps: Current policies require individuals to leave their jobs to qualify for benefits, causing financial strain while they learn new skills.

### Job Fairs
- Inadequate Opportunities: Few job fairs are held in Jammu, and the corporate and IT culture is almost nonexistent.

### Lack of Professional Platforms
- Limited Networking: Jammu lacks platforms like LinkedIn, with very few companies having offices here.

### Impact of Fare Regulation on Driver Satisfaction and Consumer Choices

- Rickshaw Drivers Affected by Electric Rickshaws and AC Buses: The introduction of electric rickshaws and AC buses has led to a decline in customers for traditional rickshaw drivers
- Need for Metered Fares: Implementing meters would ensure fair competition and attract more customers who feel they are paying a fair price.

![This is an alt text.](/images/unemp/rickshaw.webp "This is a sample image.")

## Impact on the Community
*Krishika & Keshav*

Unemployment affects more than just individuals without jobs; it impacts families, reduces community spending, and can lead to increased poverty and social unrest. Personal stories from community members illustrate the human side of this issue.

## Case Study
India experienced one of the worst coronavirus crises in the world. The pandemic induced a sharp contraction in economic activity, causing unemployment to rise and exacerbating existing gender divides in the country. Using monthly data from the Centre for Monitoring Indian Economy on subnational economies of India from January 2019 to May 2021, we find that:
- The unemployment gender gap narrowed during the COVID-19 pandemic, largely driven by male unemployment dynamics.
- The recovery in the post-lockdown periods had spillover effects on the unemployment gender gap in rural regions.
- The unemployment gender gap during the national lockdown period was narrower than during the second wave.

![This is an alt text.](/images/unemp/new.webp "This is a sample image.")

The COVID-19 pandemic has adversely impacted labor markets worldwide. According to the International Labour Organization, the working hours lost in 2020 were equivalent to 255 million full-time jobs, translating into labor income losses worth $3.7 trillion. Due to existing gender inequalities, women were more vulnerable to the economic impact of COVID-19. The sudden closure of schools and daycare centers exacerbated the burden of unpaid care on women. Women, who disproportionately represent the accommodation, food services, and retail and wholesale trade sectors, were worst hit by the pandemic. In most countries, women often work in these sectors without work protection or job guarantees, leading them to lose their livelihoods faster than men while also dealing with deteriorating mental health.

![This is an alt text.](/images/unemp/figure.webp "This is a sample image.")

India, with one of the lowest female labor force participation rates globally, saw the pandemic exacerbate pre-existing gender disparities in unemployment. Since the onset of the pandemic, women in India have been increasingly dropping out of the labor force. The greater female labor force, comprising unemployed females who are active and inactive job seekers, has been lower than the pre-pandemic average since April 2020. The number of unemployed women actively looking for jobs has also been lower than the pre-pandemic average, except for a few months. Conversely, the number of women who are unemployed but inactive in their job search has risen drastically during this period.
A recent survey by Deloitte identified that the burden of household chores and responsibility for childcare and family dependents increased exponentially for women worldwide, and more so in India due to the pandemic. The surveyed women mentioned an increase in work and caregiving responsibilities as the main reasons for considering leaving the workforce.

## Solutions and Initiatives
*Prinka Devi*
- Promoting Tourism: Leveraging Jammu’s cultural heritage and scenic beauty to create jobs and boost the local economy. Invest in tourism infrastructure and marketing to attract more visitors.
- Supporting Local Businesses and Entrepreneurship: Encourage local entrepreneurship through microfinance and business support services. Organize entrepreneurship boot camps with local business schools and colleges.

![This is an alt text.](/images/unemp/local.webp "This is a sample image.")

- Skill Development Programs: Offer training programs in partnership with educational institutions and vocational centers. Emphasize continuous learning and development to adapt to changing job market demands.

- Job Fairs and Employment Services Increase the number of job fairs and employment services to connect job seekers with employers. Develop a robust labor market information system to track employment trends.

- Policy Interventions: Advocate for policy changes to create a conducive environment for job creation and economic growth. Implement metered fares for rickshaws to ensure fair pricing and competition.

- Community Initiatives: Conduct environmental clean-up drives to create temporary jobs and improve the community’s quality of life.

## Resources for Employment Assistance in Jammu
- MGNREGA: Provides 100 days of guaranteed wage employment per year to rural households. Vital for alleviating rural poverty and supporting community development.
[MGNREGA Official Site ](https://nrega.nic.in/MGNREGA_new/Nrega_home.aspx)

- Pradhan Mantri Kaushal Vikas Yojana (PMKVY): Offers industry-relevant skill training, including soft skills, entrepreneurship, financial and digital literacy. Active in providing training and placement assistance to improve employability.
[PMKVY Official Site ](https://www.pmkvyofficial.org/)

- Startup India: Fosters a startup culture, simplifying the startup process for entrepreneurs. Supporting new businesses and contributing to job creation.
[StartUp India Official Site ](https://www.startupindia.gov.in/)

![This is an alt text.](/images/unemp/startup.webp "This is a sample image.")

- Deen Dayal Upadhyaya Grameen Kaushalya Yojana (DDU-GKY): Skill development and placement for rural youth under the Himayat initiative, fully funded by the Central Government.
[DDU-GKY Official Site ](https://ddugky.gov.in/)

- Financial Assistance for Skill Training of Persons with Disabilities: Provides skill development for people with disabilities to enhance employability. Efforts are being made to extend these opportunities to Jammu.
[Skill Training for Disablities](https://depwd.gov.in/)

- National Apprenticeship Training Scheme: Offers practical knowledge and skills through a one-year program. Enhancing practical training opportunities for youth.
[NATS Official Site ](https://mhrdnats.gov.in/) 

- Green Skill Development Programme: Develops skills for environmental preservation and sustainability. Plans to expand training programs to promote sustainable development.
[GSDP Official Site ](http://gsdp-envis.gov.in/)

## Call to Action
Everyone in our community can play a role in addressing unemployment. Whether it’s by supporting local businesses, participating in community initiatives, or advocating for policy changes, your involvement matters. Let’s come together to create a brighter future for Jammu City.

*"The best way to find yourself is to lose yourself in the service of others." - Mahatma Gandhi*

## Conclusion
By working together, we can tackle the issue of unemployment and build a stronger, more resilient community. Let’s take action today for a better tomorrow.

## Social Media Links
- Kalhan Bhat

*[LinkedIn](linkedin.com/in/kalhan-bhat-b57978287)*
*[Instagram](https://www.instagram.com/bhatkalhan/)*

    
- Ansh Sharma

*[Instagram](https://www.instagram.com/ansh.sharma_09)*

- Krishika Jalali

*[LinkedIn](linkedin.com/in/krishikajalali)*
*[Instagram](https://www.instagram.com/krishikajalali/)*

- Rushikesh Salgotra

*Not Available*

- Keshav Kumar

*Not Available*

- Prinka Devi

*Not Available*